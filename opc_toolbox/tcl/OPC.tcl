##########################################################
##     OPC Toolbox
##     OPC.tcl
##     Developed by Zhe.Peng & Longhua.Ma
##     Email:pengzhe1113@gmail.com
##     Zhejiang Univ. Hangzhou, China
##     Dec.2006
###########################################################

set server_n 0
set servername 0
set server_num 0
set server_na(0) 0
set item_n 0
set itemname 0
set item_num 0
set item_na(0) 0
set DIRE
set w .opc
catch {destroy $w}
toplevel $w
wm title $w "OPC Toolbox V1.3"
canvas $w.c -width 800 -height 450 -bg blue


ScilabEval "opc_server_n=opc_server_browse();"


####################### server frame ###########################

labelframe $w.server -width 150 -height 450 -text "OPC Server"

label $w.server.label1 -text "Loca OPC Server:" 

frame $w.server.list -width 150
scrollbar $w.server.list.yscroll -command "$w.server.list.list yview"
scrollbar $w.server.list.xscroll -orient horizontal  -command "$w.server.list.list xview"
listbox $w.server.list.list -height 15 -yscroll "$w.server.list.yscroll set" -xscroll "$w.server.list.xscroll set"

label $w.server.label2 -text "You Selected" 
entry $w.server.entry -textvariable servername
button $w.server.button1 -width 15 -height 1 -text "Connect" -command {
ScilabEval "opc_connect('$servername')"
$w.server.button1 configure -state "disabled" -bg gray 
$w.group.button configure -state "normal" -bg green
}
button $w.server.button2 -width 15 -height 1 -text "Disconnect" -command {
$w.server.button1 configure -state "normal" -bg green
$w.item.top.mid.button4 configure -state "disabled" -bg gray 
$w.item.buttom.top.button configure -state "disabled" -bg gray 
$w.item.buttom.buttom.button configure -state "disabled" -bg gray 
$w.server.button2 configure -state "disabled" -bg gray 
set autovalue 0
$w.item.buttom.top.ch configure -state "disabled"
ScilabEval "opc_disconnect()"
}


pack $w.server.label1 $w.server.list $w.server.label2 $w.server.entry $w.server.button1 $w.server.button2 -side top -pady 5
grid $w.server.list.list -row 0 -column 0 -rowspan 1 -columnspan 1 -sticky news
grid $w.server.list.yscroll -row 0 -column 1 -rowspan 1 -columnspan 1 -sticky news
grid $w.server.list.xscroll -row 1 -column 0 -rowspan 1 -columnspan 1 -sticky news

####################### group frame ###########################

labelframe $w.group -width 450 -height 50 -text "OPC Group"

label $w.group.label0 -text " " -height 8
label $w.group.label -text "OPC Group Name" 
entry $w.group.entry -textvariable opcgroup
button $w.group.button -width 15 -height 1 -text "Add" -command {
$w.group.button configure -state "disabled" -bg gray
$w.server.button1 configure -state "disabled" -bg gray
$w.item.top.mid.button1 configure -state "normal" -bg green
$w.item.top.mid.button2 configure -state "normal" -bg green
$w.item.top.mid.button3 configure -state "normal" -bg green
ScilabEval "opc_add_group('$opcgroup')"

ScilabEval "opc_item_n=opc_item_browse();"
ScilabEval "item_i=1"
after 520 ScilabEval "opc_item_num_b=size(opc_item_n)"
after 540 ScilabEval "opc_item_num_b=opc_item_num_b(1)"
after 560 ScilabEval "opc_item_n1=opc_item_num_b(1)"
after 580 ScilabEval "TCL_SetVar('item_num',opc_item_n1)"
after 600 initial_item

}

pack $w.group.label0 $w.group.label $w.group.entry $w.group.button -side left

####################### item frame ###########################

labelframe $w.item -width 450 -height 400 -text "OPC Item"

####################### item top frame ###########################

frame $w.item.top -width 450 -height 220


####################### item top left frame ###########################

frame $w.item.top.left -width 100 -height 200

label $w.item.top.left.label -text "Item Name" 

frame $w.item.top.left.list -width 150
scrollbar $w.item.top.left.list.yscroll -command "$w.item.top.left.list.list yview"
scrollbar $w.item.top.left.list.xscroll -orient horizontal  -command "$w.item.top.left.list.list xview"
listbox $w.item.top.left.list.list -height 13 -yscroll "$w.item.top.left.list.yscroll set" -xscroll "$w.item.top.left.list.xscroll set"
grid $w.item.top.left.list.list -row 0 -column 0 -rowspan 1 -columnspan 1 -sticky news
grid $w.item.top.left.list.yscroll -row 0 -column 1 -rowspan 1 -columnspan 1 -sticky news
grid $w.item.top.left.list.xscroll -row 1 -column 0 -rowspan 1 -columnspan 1 -sticky news

pack $w.item.top.left.label $w.item.top.left.list -side top

####################### item top mid frame ###########################

frame $w.item.top.mid -width 60 -height 200

button $w.item.top.mid.button1 -width 3 -height 1 -text ">"  -command {
global w
set write_item_name [$w.item.top.left.list.list get [$w.item.top.left.list.list curselection] ]
$w.item.top.right.list.list insert end $write_item_name
}

button $w.item.top.mid.button2 -width 3 -height 1 -text "<" -command {
global w
$w.item.top.right.list.list delete [$w.item.top.right.list.list curselection]
}

button $w.item.top.mid.button3 -width 3 -height 1 -text "OK"  -command {

$w.item.top.mid.button1 configure -state "disabled" -bg gray
$w.item.top.mid.button2 configure -state "disabled" -bg gray
$w.item.top.mid.button3 configure -state "disabled" -bg gray
$w.item.top.mid.button4 configure -state "normal" -bg green
$w.item.buttom.top.button configure -state "normal" -bg green
$w.item.buttom.buttom.button configure -state "normal" -bg green
$w.item.buttom.top.ch configure -state "normal"
$w.server.button2 configure -state "normal" -bg green
set item_t [$w.item.top.right.list.list index end]
for {set list_num_temp 0} {$list_num_temp<[$w.item.top.right.list.list index end]} {incr list_num_temp 1} {
ScilabEval "item($list_num_temp+1)='[$w.item.top.right.list.list get $list_num_temp]'"
ScilabEval "item_num_$list_num_temp=$list_num_temp"
}

ScilabEval "item_num=$item_t"
ScilabEval "opc_add_item(item,item_num)"

}
 
button $w.item.top.mid.button4 -width 3 -height 1 -text "Edit"  -command {
$w.item.top.mid.button1 configure -state "normal" -bg green
$w.item.top.mid.button2 configure -state "normal" -bg green
$w.item.top.mid.button3 configure -state "normal" -bg green
set autovalue 0
$w.item.buttom.top.ch configure -state "disabled"
}

pack $w.item.top.mid.button1 $w.item.top.mid.button2 $w.item.top.mid.button3 $w.item.top.mid.button4 -side top -pady 1

####################### item top right frame ###########################

frame $w.item.top.right -width 100 -height 200

label $w.item.top.right.label -text "Item" 

frame $w.item.top.right.list -width 150
scrollbar $w.item.top.right.list.yscroll -command "$w.item.top.right.list.list yview"
scrollbar $w.item.top.right.list.xscroll -orient horizontal  -command "$w.item.top.right.list.list xview"
listbox $w.item.top.right.list.list -height 13 -yscroll "$w.item.top.right.list.yscroll set" -xscroll "$w.item.top.right.list.xscroll set"
grid $w.item.top.right.list.list -row 0 -column 0 -rowspan 1 -columnspan 1 -sticky news
grid $w.item.top.right.list.yscroll -row 0 -column 1 -rowspan 1 -columnspan 1 -sticky news
grid $w.item.top.right.list.xscroll -row 1 -column 0 -rowspan 1 -columnspan 1 -sticky news

pack $w.item.top.right.label $w.item.top.right.list -side top




####################### item top right2 frame ###########################

frame $w.item.top.right2 -width 100 -height 200

label $w.item.top.right2.label -text "Value" 

frame $w.item.top.right2.list -width 150
scrollbar $w.item.top.right2.list.yscroll -command "$w.item.top.right2.list.list yview"
scrollbar $w.item.top.right2.list.xscroll -orient horizontal  -command "$w.item.top.right2.list.list xview"
listbox $w.item.top.right2.list.list -height 13 -yscroll "$w.item.top.right2.list.yscroll set" -xscroll "$w.item.top.right2.list.xscroll set"
grid $w.item.top.right2.list.list -row 0 -column 0 -rowspan 1 -columnspan 1 -sticky news
grid $w.item.top.right2.list.yscroll -row 0 -column 1 -rowspan 1 -columnspan 1 -sticky news
grid $w.item.top.right2.list.xscroll -row 1 -column 0 -rowspan 1 -columnspan 1 -sticky news

pack $w.item.top.right2.label $w.item.top.right2.list -side top


pack $w.item.top.left $w.item.top.mid $w.item.top.right $w.item.top.right2 -side left -padx 3


####################### item buttom frame ###########################

frame $w.item.buttom -width 450 -height 150

####################### item buttom top1 frame ###########################

labelframe $w.item.buttom.top1 -width 450 -height 50 -text "Item Add Mannual"

entry $w.item.buttom.top1.ch -textvariable item_mannual -width 20
button $w.item.buttom.top1.button -width 20 -height 1 -text "Add Mannual Item"  -command {

$w.item.top.right.list.list insert end $item_mannual

}


pack $w.item.buttom.top1.ch $w.item.buttom.top1.button -side left -padx 10

####################### item buttom top frame ###########################

labelframe $w.item.buttom.top -width 450 -height 50 -text "Item Read"

checkbutton $w.item.buttom.top.ch -text "Auto Read" -variable autovalue -relief flat
button $w.item.buttom.top.button -width 10 -height 1 -text "Read"  -command {
$w.item.top.right2.list.list delete 0 [$w.item.top.right2.list.list index end]
ScilabEval {item_read_result=opc_item_read(item_num,'f')}

for {set list_num_temp 0} {$list_num_temp<[$w.item.top.right.list.list index end]} {incr list_num_temp 1} {
ScilabEval "item_read_i=$list_num_temp+1"
ScilabEval "TCL_SetVar('item_read_temp($list_num_temp)',item_read_result(item_read_i))"
$w.item.top.right2.list.list insert end $item_read_temp($list_num_temp)
}
}


pack $w.item.buttom.top.ch $w.item.buttom.top.button -side left -padx 10

####################### item buttom buttom frame ###########################

labelframe $w.item.buttom.buttom -width 450 -height 50 -text "Item Write"

####################### item buttom buttom t frame ###########################

frame $w.item.buttom.buttom.t -width 300 -height 45

label $w.item.buttom.buttom.label1 -text "Item Name" 
entry $w.item.buttom.buttom.entry1 -textvariable write_item_name -width 20
entry $w.item.buttom.buttom.entry2 -textvariable write_item_i -width 2
label $w.item.buttom.buttom.label2 -text "Item Value" 
entry $w.item.buttom.buttom.entry3 -textvariable write_item_value -width 8
button $w.item.buttom.buttom.button -width 15 -height 1 -text "Write"  -width 8 -command {
ScilabEval "item_write_i=$write_item_i"
ScilabEval "item_write_v=$write_item_value"
ScilabEval "opc_item_write(item_write_i,item_write_v,'f')"
} 

pack $w.item.buttom.buttom.label1 $w.item.buttom.buttom.entry1 $w.item.buttom.buttom.entry2 $w.item.buttom.buttom.label2  $w.item.buttom.buttom.entry3 $w.item.buttom.buttom.button -side left

pack $w.item.buttom.top1 $w.item.buttom.top $w.item.buttom.buttom -side top -pady 1
pack $w.item.top $w.item.buttom -side top -pady 2


pack $w.server -side left
pack $w.group $w.item -side top -padx 5

foreach value {server group item item.top item.buttom item.buttom.top1 item.buttom.top item.buttom.buttom} {
    pack propagate $w.$value false
}


proc initial_list {} {
global w
global server_na
global server_num
set item_tt [$w.server.list.list index end]
$w.server.list.list delete 0 [$w.server.list.list index end]
ScilabEval "if exists('opc_server_num') then opc_server_num=opc_server_num(1),end"
ScilabEval "item_read_i=1"
for {set list_num_temp 0} {$list_num_temp<$server_num} {incr list_num_temp 1} {
ScilabEval "item_read_i=$list_num_temp+1"
ScilabEval "TCL_SetVar('server_na($list_num_temp)',opc_server_n(item_read_i))"
}
after 100 add_list
}

proc add_list {} {
global w
global server_na
global server_num
global servername
set servername $server_na(0)
for {set list_num_temp 0} {$list_num_temp<$server_num-1} {incr list_num_temp 1} {
$w.server.list.list insert end $server_na($list_num_temp)
}
}

proc initial_item {} {
global w
global item_na
global item_num
set item_tt [$w.item.top.left.list.list index end]
$w.item.top.left.list.list delete 0 [$w.item.top.left.list.list index end]
ScilabEval "if exists('opc_server_num') then opc_server_num=opc_server_num(1),end"
ScilabEval "item_read_i=1"
for {set list_num_temp 0} {$list_num_temp<$item_num} {incr list_num_temp 1} {
ScilabEval "item_read_i=$list_num_temp+1"
ScilabEval "TCL_SetVar('item_na($list_num_temp)',opc_item_n(item_read_i))"
}
after 200 add_item
}

proc add_item {} {
global w
global item_na
global item_num
global itemname
for {set list_num_temp 0} {$list_num_temp<$item_num-1} {incr list_num_temp 1} {
$w.item.top.left.list.list insert end $item_na($list_num_temp)
}
}


ScilabEval "item_i=1"
ScilabEval "opc_server_num=size(opc_server_n)"
after 110 ScilabEval "opc_server_num=opc_server_num(1)"
after 120 ScilabEval "opc_server_n1=opc_server_n(1)"
after 130 ScilabEval "TCL_SetVar('server_num',opc_server_num)"
after 140 initial_list



####################### bind mouse ###########################

bind $w.server.list.list <ButtonRelease-1> {
set servername [$w.server.list.list get [$w.server.list.list curselection] ]
}

bind $w.item.top.right.list.list <ButtonRelease-1> {
set write_item_i [$w.item.top.right.list.list curselection]
set write_item_name [$w.item.top.right.list.list get [$w.item.top.right.list.list curselection] ]
}


set opcgroup Group1
$w.server.button1 configure -state "normal" -bg green
$w.server.button2 configure -state "disabled"
$w.group.button configure -state "disabled"
$w.item.top.mid.button1 configure -state "disabled"
$w.item.top.mid.button2 configure -state "disabled"
$w.item.top.mid.button3 configure -state "disabled"
$w.item.top.mid.button4 configure -state "disabled"
$w.item.buttom.top.button configure -state "disabled"
$w.item.buttom.buttom.button configure -state "disabled"
$w.server.entry configure -state "disabled"
$w.item.buttom.top.ch configure -state "disabled"


proc time_loop {} {
global w
global list_num_temp
global item_read_temp
global autovalue

if {$autovalue==1} {

$w.item.top.right2.list.list delete 0 [$w.item.top.right2.list.list index end]
ScilabEval {item_read_result=opc_item_read(item_num,'f')}
for {set list_num_temp 0} {$list_num_temp<[$w.item.top.right.list.list index end]} {incr list_num_temp 1} {
ScilabEval "item_read_i=$list_num_temp+1"
ScilabEval "TCL_SetVar('item_read_temp($list_num_temp)',item_read_result(item_read_i))"
$w.item.top.right2.list.list insert end $item_read_temp($list_num_temp)
}

} else {}

after 1000 time_loop
}

after 1000 time_loop

