//---------------------------------------------------------------------------
// OPC Client for Scilab
// Developed by Zhe.Peng & Longhua.Ma
// updated by Allan CORNET for Scilab 5.2 and more
// Allan CORNET - 2011 - DIGITEO - Version 1.4 for Scilab 5.3 and more
//---------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <OPCDA.h>
#include <OPCError.h>
#include <opcda_i.c>
#include <string.h>
#include <ATLCONV.H>
#include <comutil.h>
#include "opc_client.h"
//---------------------------------------------------------------------------
#pragma comment(lib, "comsuppw.lib")
//---------------------------------------------------------------------------
#define LOCALE_ID    0x409	// Code 0x409 = ENGLISH
#define OPC_ACCESS_READ 1
#define OPC_ACCESS_WRITE 2
//---------------------------------------------------------------------------
IOPCServer  *m_IOPCServer = NULL;
IOPCBrowseServerAddressSpace *pIOPCBrowse = NULL;
IOPCItemMgt		*m_IOPCItemMgt = NULL;
IOPCSyncIO		*m_IOPCSyncIO = NULL;
OPCITEMDEF		m_Items[OPC_STRING_LENGTH_MAX];
OPCITEMRESULT	*m_ItemResult = NULL;
OPCHANDLE		m_GrpSrvHandle;
HRESULT			*m_pErrors = NULL;
double m_readValue = 0.0;
HRESULT		r1;
CLSID		clsid;
LONG		TimeBias = 0;
FLOAT		PercentDeadband = 0.0;
DWORD		RevisedUpdateRate = 0;
char item_name[OPC_NB_ITEMS_MAX][OPC_STRING_LENGTH_MAX];
int item_num = 0;
//---------------------------------------------------------------------------
static opc_client_error opc_read(int readType,
                                 int *value_b, float *value_f, double *value_d,
                                 int *value_i, wchar_t **value_c,
                                 int item_index);
static opc_client_error opc_write(int readType,
                                  const int value_b, const float value_f, const double value_d,
                                  const int value_i,  const wchar_t *value_c,
                                  int item_index);
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_connect(const char *server) 
{
    if (*server == NULL)
    {
        return OPC_CLIENT_ERROR_NOK;
    }

    r1 = CoInitialize(NULL);

    if (r1 != S_OK)
    {	
        if (r1 != S_FALSE)
        {
            return OPC_CLIENT_ERROR_CONNECT_COM_INITIALIZE_FAILED;
        }
    }

    USES_CONVERSION; 
    r1 = CLSIDFromProgID(A2W(server), &clsid); 
    if (r1 != S_OK)
    {	
        CoUninitialize();
        return OPC_CLIENT_ERROR_CONNECT_CLSID_FAILURE;
    }

    r1 = CoCreateInstance (clsid, NULL, CLSCTX_LOCAL_SERVER, IID_IOPCServer, (void**)&m_IOPCServer);
    if (r1 != S_OK)
    {	
        m_IOPCServer = NULL;
        CoUninitialize();
        return OPC_CLIENT_ERROR_CONNECT_CREATE_INSTANCE_FAILURE;
    }
    return OPC_CLIENT_ERROR_OK;
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_add_group(const char *group)
{
    opc_client_error returnedError = OPC_CLIENT_ERROR_OK;

    if (*group == NULL)
    {
        return OPC_CLIENT_ERROR_NOK;
    }

    USES_CONVERSION; 
    r1 = m_IOPCServer->AddGroup(A2W(group),			//	[in] group name
        TRUE,					//	[in] active
        OPC_UPDATE_RATE,					//	[in] request this Update Rate from Server
        OPC_ONE,				//	[in] Client handle
        &TimeBias,				//	[in] no time interval to system UTC time
        &PercentDeadband,		//	[in] no deadband, so all data changes are reported	
        LOCALE_ID,				//	[in] Server uses English language for text values
        &m_GrpSrvHandle,		//	[out] Server handle to identify this group in later calls
        &RevisedUpdateRate,		//	[out] the answer form the Server to the requested update rate
        IID_IOPCItemMgt,		//	[in] requested interface type of the group object
        (LPUNKNOWN*)&m_IOPCItemMgt);	//	[out] pointer to the requested interface

    if (r1 == OPC_S_UNSUPPORTEDRATE)
    {	
        returnedError = OPC_CLIENT_ERROR_ADD_GROUP_SPEED_ERROR;
    }
    else if (FAILED(r1))
    {
        m_IOPCServer->Release();
        m_IOPCServer = NULL;
        CoUninitialize();
        returnedError = OPC_CLIENT_ERROR_ADD_GROUP_FAILURE;
    }
    return returnedError;
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error copy_item(int index_item, const char *item)
{
    opc_client_error returnedError = OPC_CLIENT_ERROR_NOK;
    if (*item)
    {
        if ((index_item >= 0) && (index_item < OPC_NB_ITEMS_MAX))
        {
            memset(item_name[index_item], 0, OPC_STRING_LENGTH_MAX);
            if ((int)strlen(item) > OPC_STRING_LENGTH_MAX - 1)
            {
                strncpy(item_name[index_item], item, OPC_STRING_LENGTH_MAX - 1);
                returnedError = OPC_CLIENT_ERROR_COPY_ITEM_TRUNCATE;
            }
            else
            {
                strcpy(item_name[index_item], item);
                returnedError = OPC_CLIENT_ERROR_OK;
            }
        }
    }
    return returnedError;
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_add_item(int item_i) 
{
    opc_client_error returnedError = OPC_CLIENT_ERROR_OK;

    if ((item_i < 0) || (item_i > OPC_NB_ITEMS_MAX))
    {
        return OPC_CLIENT_ERROR_NOK;
    }
    m_ItemResult = NULL;
    item_num = item_i;

    for(int item_i_temp = 0; item_i_temp < item_i; item_i_temp++)
    {
        m_Items[item_i_temp].szAccessPath = L"";			//	Accesspath not needed
        USES_CONVERSION; 
        m_Items[item_i_temp].szItemID = A2W(item_name[item_i_temp]);		//	ItemID, see above
        m_Items[item_i_temp].bActive = TRUE;			
        m_Items[item_i_temp].hClient = OPC_ONE;
        m_Items[item_i_temp].dwBlobSize = 0;
        m_Items[item_i_temp].pBlob = NULL;
        m_Items[item_i_temp].vtRequestedDataType = 0;				//	return values in native (cannonical) datatype 
        //	defined by the item itself
    }

    r1 = m_IOPCItemMgt->AddItems(item_i,				   //	[in] add one item
        m_Items,				//	[in] see above
        &m_ItemResult,			//	[out] array with additional information about the item
        &m_pErrors);			//	[out] tells which of the items was successfully added.
    //	For any item which failed it provides a reason

    if ( (r1 != S_OK) && (r1 != S_FALSE) )
    {	
        m_IOPCItemMgt->Release();
        m_IOPCItemMgt = NULL;
        m_GrpSrvHandle = NULL;
        m_IOPCServer->Release();
        m_IOPCServer = NULL;
        CoUninitialize();		
        return OPC_CLIENT_ERROR_ADD_ITEM_FAILURE;
    }

    for(int item_write_read = 0; item_write_read < item_i; item_write_read++)
    {
        if (m_ItemResult[item_write_read].dwAccessRights != (OPC_READABLE + OPC_WRITEABLE))
        {
            returnedError = OPC_CLIENT_ERROR_ADD_ITEM_ACCESSRIGHTS;
            break;
        }
    }

    r1 = m_IOPCItemMgt->QueryInterface(IID_IOPCSyncIO, (void**)&m_IOPCSyncIO);
    if (r1 < 0)
    {	
        if (returnedError != OPC_CLIENT_ERROR_ADD_ITEM_ACCESSRIGHTS)
        {
            returnedError = OPC_CLIENT_ERROR_ADD_ITEM_QUERYINTERFACE;
        }
        CoTaskMemFree(m_ItemResult);
        m_IOPCItemMgt->Release();
        m_IOPCItemMgt = NULL;
        m_GrpSrvHandle = NULL;
        m_IOPCServer->Release();
        m_IOPCServer = NULL;
        CoUninitialize();
    }
    return returnedError;
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_item_browse(char **item_n, int nbItem_n)
{
    opc_client_error returnedError = OPC_CLIENT_ERROR_OK;
    HRESULT r0;
    HRESULT r11;
    OPCBROWSETYPE brType = OPC_FLAT;
    LPCWSTR szFilt = L"*";
    VARTYPE vtFilt = VT_EMPTY; 
    DWORD arFilt = OPC_ACCESS_READ;
    IEnumString *pEnum = NULL;
    

    pIOPCBrowse = NULL;

    r0 = m_IOPCServer->QueryInterface(IID_IOPCServer, (void**)&m_IOPCServer);
    r0 = m_IOPCServer->QueryInterface(IID_IOPCBrowseServerAddressSpace, (void**)&pIOPCBrowse);

    if (FAILED(r0))
    {
        return OPC_CLIENT_ERROR_ITEM_BROWSE_NOT_EXIST; 
    }

    r11 = pIOPCBrowse->BrowseOPCItemIDs(brType, szFilt, vtFilt, arFilt, &pEnum);
    if (FAILED(r11))
    {
        return OPC_CLIENT_ERROR_ITEM_BROWSE_IDS;
    }
    else
    {
        HRESULT r2 = S_OK;
        LPOLESTR pStr;
        LPOLESTR pName;
        ULONG actual;

        if (pEnum) 
        {
            int i = 0;
            while((r2 = pEnum->Next(OPC_ONE, &pStr, &actual)) == S_OK) 
            {
                pIOPCBrowse->GetItemID(pStr, &pName);
                if (i < nbItem_n)
                {
                    strcpy(item_n[i++], _com_util::ConvertBSTRToString(pName));
                }
            }

            if (FAILED(r2)) 
            {
                returnedError = OPC_CLIENT_ERROR_ITEM_BROWSE_NEXT;
            }

            pEnum->Release(); 
        }
        else 
        {
            returnedError = OPC_CLIENT_ERROR_ITEM_BROWSE_NULL;
        }
    }
    return returnedError;
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP char **opc_create_server_array(int nbServer)
{
    char **server = NULL;
    if (nbServer > 0)
    {
        server = (char**)malloc(sizeof(char*) * nbServer);
        if (server)
        {
            int i = 0;
            for(i = 0; i < nbServer; i++)
            {
                server[i] = (char*)malloc(sizeof(char) * OPC_STRING_LENGTH_MAX);
                if (server[i] == NULL)
                {
                    return server = opc_free_server_array(server, i);
                }
                else
                {
                    memset(server[i], 0, OPC_STRING_LENGTH_MAX);
                }
            }
        }
    }
    return server;
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP char **opc_create_item_array(int nbItem_n)
{
    char **item = NULL;
    if (nbItem_n > 0)
    {
        item = (char**)malloc(sizeof(char*) * nbItem_n);
        if (item)
        {
            int i = 0;
            for(i = 0; i < nbItem_n; i++)
            {
                item[i] = (char*)malloc(sizeof(char) * OPC_STRING_LENGTH_MAX);
                if (item[i] == NULL)
                {
                    return item = opc_free_item_array(item, i);
                }
                else
                {
                    memset(item[i], 0, OPC_STRING_LENGTH_MAX);
                }
            }
        }
    }
    return item;
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP char **opc_free_server_array(char **server, int nbServer)
{
    if (server)
    {
        if (nbServer > 0)
        {
            int i = 0;
            for(i = 0; i < nbServer; i++)
            {
                if (server[i])
                {
                    free(server[i]);
                    server[i] = NULL;
                }
            }
            free(server);
            server = NULL;
        }
    }
    return server;
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP char **opc_free_item_array(char **item_n, int nbItem_n)
{
    if (item_n)
    {
        if (nbItem_n > 0)
        {
            int i = 0;
            for(i = 0; i < nbItem_n; i++)
            {
                if (item_n[i])
                {
                    free(item_n[i]);
                    item_n[i] = NULL;
                }
            }
            free(item_n);
            item_n = NULL;
        }
    }
    return item_n;
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_read_boolean(int *value_b, int item_index)
{
    return opc_read(VT_BOOL,
                    value_b, NULL, NULL,
                    NULL, NULL,
                    item_index);
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_read_float(float *value_f, int item_index)
{
    return opc_read(VT_R4,
                    NULL, value_f, NULL,
                    NULL, NULL,
                    item_index);
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_read_double(double *value_d, int item_index)
{
    return opc_read(VT_R8,
                    NULL, NULL, value_d,
                    NULL, NULL,
                    item_index);
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_read_integer(int *value_i, int item_index)
{
    return opc_read(VT_INT,
                    NULL, NULL, NULL,
                    value_i, NULL,
                    item_index);
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_read_string(wchar_t **value_c, int item_index)
{
    return opc_read(VT_BSTR,
                    NULL, NULL, NULL,
                    NULL, value_c,
                    item_index);
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_write_boolean(const int value_b, int item_index)
{
    return opc_write(VT_BOOL, 
                    value_b, NULL, NULL,
                    NULL, NULL,
                    item_index);
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_write_float(const float value_f, int item_index)
{
    return opc_write(VT_R4, 
                     NULL, value_f, NULL,
                     NULL, NULL,
                     item_index);
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_write_double(const double value_d, int item_index)
{
    return opc_write(VT_R8, 
                     NULL, NULL, value_d,
                     NULL, NULL,
                     item_index);
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_write_integer(const int value_i, int item_index)
{
    return opc_write(VT_INT, 
                     NULL, NULL, NULL,
                     value_i, NULL,
                     item_index);
}
//---------------------------------------------------------------------------
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_write_string(const wchar_t *value_c, int item_index)
{
    return opc_write(VT_BSTR, 
                     NULL, NULL, NULL,
                     NULL, value_c,
                     item_index);
}
//---------------------------------------------------------------------------
static opc_client_error opc_read(int readType,
                                 int *value_b, float *value_f, double *value_d,
                                 int *value_i, wchar_t **value_c,
                                 int item_index)
{
    opc_client_error returnedError = OPC_CLIENT_ERROR_NOK;
    OPCHANDLE		*phServer = NULL;
    OPCITEMSTATE	*pItemValue = NULL;
    HRESULT			*pErrors = NULL;
    HRESULT			r1;
    UINT			qnr = 0;

    if ((item_index < 0) || (item_index > OPC_NB_ITEMS_MAX))
    {
        return returnedError;
    }

    if (m_pErrors[0] != S_OK)		
    {	
        return OPC_CLIENT_ERROR_READ_SYNC_RW;
    }

    phServer = new OPCHANDLE[item_index];

    for(int item_write_read = 0; item_write_read < item_index; item_write_read++)
    {
        phServer[item_write_read] = m_ItemResult[item_write_read].hServer;
    }

    r1 = m_IOPCSyncIO->Read(OPC_DS_DEVICE, item_index, phServer, &pItemValue, &pErrors);
    delete [] phServer;

    if (r1 == S_OK) 
    {
        for(int item_write_read = 0; item_write_read < item_index; item_write_read++)
        {
            switch (readType)
            {
            case VT_R4:
                value_f[item_write_read] = (float)pItemValue[item_write_read].vDataValue.fltVal;	
                break;
            case VT_R8:
                value_d[item_write_read] = (double)pItemValue[item_write_read].vDataValue.dblVal;	
                break;
            case VT_I4:
                value_i[item_write_read] = (int)pItemValue[item_write_read].vDataValue.intVal;	
                break;
            case VT_BOOL:
                value_b[item_write_read] = (int)pItemValue[item_write_read].vDataValue.boolVal;	
                break;
            case VT_BSTR:
                wcsncpy(value_c[item_write_read], pItemValue[item_write_read].vDataValue.bstrVal, OPC_STRING_LENGTH_MAX);
                break;
            default:
                return OPC_CLIENT_ERROR_READ_ERROR;
                break;
            }
            qnr = pItemValue[item_write_read].wQuality;

            switch(qnr)   
            {   
            case OPC_QUALITY_GOOD:
                break;
            case OPC_QUALITY_BAD:   
            default:   
                returnedError = OPC_CLIENT_ERROR_READ_QUALITY_BAD;
                break;   
            case OPC_QUALITY_UNCERTAIN:   
                returnedError = OPC_CLIENT_ERROR_READ_QUALITY_UNCERTAIN;
                break;   
            case OPC_QUALITY_CONFIG_ERROR:   
                returnedError = OPC_CLIENT_ERROR_READ_CONFIG_ERROR;
                break;   
            case OPC_QUALITY_NOT_CONNECTED:   
                returnedError = OPC_CLIENT_ERROR_READ_NOT_CONNECTED;
                break;   
            case OPC_QUALITY_DEVICE_FAILURE:   
                returnedError = OPC_CLIENT_ERROR_READ_DEVICE_FAILURE;
                break;   
            case OPC_QUALITY_OUT_OF_SERVICE:   
                returnedError = OPC_CLIENT_ERROR_READ_OUT_OF_SERVICE;
                break;   
            }   
        }
    }

    if (r1 == S_FALSE) 
    {	
        returnedError = OPC_CLIENT_ERROR_READ_ERROR;
    }
    if (FAILED(r1)) 
    {
        returnedError = OPC_CLIENT_ERROR_READ_FAILURE;
    }
    else 
    {
        CoTaskMemFree(pErrors);
        CoTaskMemFree(pItemValue);
    }
    return returnedError;
}
//---------------------------------------------------------------------------
static opc_client_error opc_write(int writeType,
                                  const int value_b, const float value_f, const double value_d,
                                  const int value_i,  const wchar_t *value_c,
                                  int item_index)
{
    opc_client_error returnedError = OPC_CLIENT_ERROR_NOK;
    OPCHANDLE		*phServer = NULL;
    HRESULT			*pErrors = NULL;
    VARIANT			value;
    HRESULT			r1;
    LPWSTR			ErrorStr;

    if ((item_index < 0) || (item_index > OPC_NB_ITEMS_MAX))
    {
        return returnedError;
    }

    value.vt = VT_EMPTY;

    if (m_pErrors[item_index] != S_OK)		//Item not available
    {	
        return OPC_CLIENT_ERROR_WRITE_SYNC_RW;
    }	

    phServer = new OPCHANDLE[OPC_ONE];
    phServer[0] = m_ItemResult[item_index].hServer;      

    switch (writeType)
    {
    case VT_R4:
        value.vt = VT_R4;
        value.fltVal = value_f;
        break;
    case VT_R8:
        value.vt = VT_R8;
        value.dblVal = value_d;
        break;
    case VT_I4:
        value.vt = VT_I4;
        value.intVal = value_i;
        break;
    case VT_BOOL:
        value.vt = VT_BOOL;
        value.boolVal = (VARIANT_BOOL)value_b;
        break;
    case VT_BSTR:
        value.vt = VT_BSTR;
        value.bstrVal = SysAllocString(value_c);
        wcsncpy(value.bstrVal, value_c, OPC_STRING_LENGTH_MAX);
        break;
    default:
        break;
    }


    r1 = m_IOPCSyncIO->Write(OPC_ONE, phServer, &value, &pErrors);
    if (writeType == VT_BSTR)
    {
        SysFreeString(value.bstrVal);
    }

    delete[] phServer;

    if (FAILED(r1))
    {		
        returnedError = OPC_CLIENT_ERROR_WRITE_ERROR;
    }
    else 
    {
        m_IOPCServer->GetErrorString(pErrors[item_index], LOCALE_ID, &ErrorStr);
        CoTaskMemFree(pErrors);
        returnedError = OPC_CLIENT_ERROR_OK;
    }
    return returnedError;
}
//---------------------------------------------------------------------------