//---------------------------------------------------------------------------
// Allan CORNET - 2011 - DIGITEO - Version 1.4 for Scilab 5.3 and more
//---------------------------------------------------------------------------

#ifndef __OPC_CLIENT_H__
#define __OPC_CLIENT_H__

#include <Windows.h>

#define OPC_STRING_LENGTH_MAX 4096
#define OPC_NB_ITEMS_MAX 100
#define OPC_NB_SERVERS_MAX 100
#define OPC_ONE 1
#define OPC_STR_KEY "OPC"
#define OPC_UPDATE_RATE 500

typedef enum {
    OPC_CLIENT_ERROR_OK = 0,
    OPC_CLIENT_ERROR_NOK = 1,
    OPC_CLIENT_ERROR_CONNECT_COM_INITIALIZE_FAILED = 2,
    OPC_CLIENT_ERROR_CONNECT_CLSID_FAILURE = 3,
    OPC_CLIENT_ERROR_CONNECT_CREATE_INSTANCE_FAILURE = 4,
    OPC_CLIENT_ERROR_ADD_GROUP_SPEED_ERROR = 5,
    OPC_CLIENT_ERROR_ADD_GROUP_FAILURE = 6,
    OPC_CLIENT_ERROR_COPY_ITEM_TRUNCATE = 7,
    OPC_CLIENT_ERROR_ADD_ITEM_FAILURE = 8,
    OPC_CLIENT_ERROR_ADD_ITEM_ACCESSRIGHTS = 9,
    OPC_CLIENT_ERROR_ADD_ITEM_QUERYINTERFACE = 10,
    OPC_CLIENT_ERROR_READ_SYNC_RW = 11,
    OPC_CLIENT_ERROR_READ_ERROR = 12,
    OPC_CLIENT_ERROR_READ_FAILURE = 13,
    OPC_CLIENT_ERROR_READ_QUALITY_BAD = 14,
    OPC_CLIENT_ERROR_READ_QUALITY_UNCERTAIN = 15,
    OPC_CLIENT_ERROR_READ_CONFIG_ERROR = 16,
    OPC_CLIENT_ERROR_READ_NOT_CONNECTED = 17,
    OPC_CLIENT_ERROR_READ_DEVICE_FAILURE = 18,
    OPC_CLIENT_ERROR_READ_OUT_OF_SERVICE = 19,
    OPC_CLIENT_ERROR_WRITE_SYNC_RW = 20,
    OPC_CLIENT_ERROR_WRITE_ERROR = 21,
    OPC_CLIENT_ERROR_DISCONNECT_REMOVE_ITEMS_FAILURE = 22,
    OPC_CLIENT_ERROR_DISCONNECT_REMOVE_GROUP_FAILURE = 23,
    OPC_CLIENT_ERROR_ITEM_BROWSE_NOT_EXIST = 24,
    OPC_CLIENT_ERROR_ITEM_BROWSE_IDS = 25,
    OPC_CLIENT_ERROR_ITEM_BROWSE_NEXT = 26,
    OPC_CLIENT_ERROR_ITEM_BROWSE_NULL = 27,
    
} opc_client_error;

#ifdef OPC_CLIENT_EXPORTS
#define OPC_CLIENT_IMPEXP _declspec(dllexport)
#else
#define OPC_CLIENT_IMPEXP _declspec(dllimport)
#endif
//
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_connect(const char *server); 
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_add_group(const char *group); 
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error copy_item(int index_item, const char *item);
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_add_item(int item_i);
//
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_read_boolean(int *value_b, int item_index);
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_read_float(float *value_f, int item_index);
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_read_double(double *value_d, int item_index);
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_read_integer(int *value_i, int item_index);
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_read_string(wchar_t **value_c, int item_index);
//
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_write_boolean(const int value_b, int item_index);
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_write_float(const float value_f, int item_index);
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_write_double(const double value_d, int item_index);
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_write_integer(const int value_i, int item_index);
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_write_string(const wchar_t *value_c, int item_index);
//
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_disconnect(void);
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_server_browse(char **server, int nbServerMax, int *nbServerFounded);
EXTERN_C OPC_CLIENT_IMPEXP opc_client_error opc_item_browse(char **item_n, int nbItem_n);
//
EXTERN_C OPC_CLIENT_IMPEXP char **opc_create_server_array(int nbServer);
EXTERN_C OPC_CLIENT_IMPEXP char **opc_create_item_array(int nbItem_n);
EXTERN_C OPC_CLIENT_IMPEXP char **opc_free_server_array(char **server, int nbServer);
EXTERN_C OPC_CLIENT_IMPEXP char **opc_free_item_array(char **item_n, int nbItem_n);
//

#endif /* __OPC_CLIENT_H__ */
