// Copyright 2011 - DIGITEO - Allan CORNET


function builder_gateway_opc()

sci_gateway_dir = get_absolute_file_path("builder_gateway.sce");
languages       = ["c"];

tbx_builder_gateway_lang(languages,sci_gateway_dir);
tbx_build_gateway_loader(languages,sci_gateway_dir);
tbx_build_gateway_clean(languages,sci_gateway_dir);

endfunction

builder_gateway_opc();
clear builder_gateway_opc;