// Copyright 2011 - DIGITEO - Allan CORNET

function builder_gateway_c_opc()

gw_src_c = get_absolute_file_path("builder_gateway_c.sce");

includes_src_c = "-I""" + gw_src_c + "../../src/cpp""";

// PutLhsVar managed by user 
// if you do not this variable, PutLhsVar is added
// in gateway generated (default mode in scilab 4.x and 5.x)
WITHOUT_AUTO_PUTLHSVAR = %t;

functions_list = ["opc_add_group"     , "sci_opc_add_group"; ..
                  "opc_add_item"      , "sci_opc_add_item"; ..
                  "opc_connect"       , "sci_opc_connect"; ..
                  "opc_disconnect"    , "sci_opc_disconnect"; ..
                  "opc_item_browse"   , "sci_opc_item_browse"; ..
                  "opc_item_read"     , "sci_opc_item_read"; ..
                  "opc_item_write"    , "sci_opc_item_write"; ..
                  "opc_server_browse" , "sci_opc_server_browse"; ..
];


files_list = ["sci_opc_add_group.c", ..
							"sci_opc_add_item.c", ..
							"sci_opc_connect.c", ..
							"sci_opc_disconnect.c", ..
							"sci_opc_item_browse.c", ..
							"sci_opc_item_read.c", ..
							"sci_opc_item_write.c", ..
							"sci_opc_server_browse.c", ..

];

lib_src = "";

if win64() then
  lib_src = "../../bin/opc_client_x64";
else
  lib_src = "../../bin/opc_client_x86";
end

tbx_build_gateway("gw_opc_client", ..
                  functions_list, ..
                  files_list, ..
                  gw_src_c, ..
                  lib_src, ..
                  "", ..
                  includes_src_c);

endfunction

builder_gateway_c_opc();
clear builder_gateway_c_opc;
