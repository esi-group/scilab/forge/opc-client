/* ========================================================================== */
/* Allan CORNET */
/* DIGITEO 2011 */
/* ========================================================================== */
#include "opc_client.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "localization.h"
/* ========================================================================== */
int sci_opc_server_browse(char *fname)
{
    char **servers = NULL;

    Rhs = max(Rhs, 0);
    CheckRhs(0, 0);
    CheckLhs(1, 1);

    servers = opc_create_server_array(OPC_NB_SERVERS_MAX);
    if (servers != NULL)
    {
        int nbServers = 0;
        opc_client_error opc_error = opc_server_browse(servers, OPC_NB_SERVERS_MAX, &nbServers);
        if (opc_error != OPC_CLIENT_ERROR_OK)
        {
            SciErr sciErr = createMatrixOfString(pvApiCtx, Rhs + 1, nbServers, 1, servers);
            if(sciErr.iErr)
            {
                printError(&sciErr, 0);
                return 0;
            }
            LhsVar(1) = Rhs + 1;
            C2F(putlhsvar)();
        }
        servers = opc_free_server_array(servers, OPC_NB_SERVERS_MAX);
    }
    else
    {

    }
    return 0;
}
/* ========================================================================== */
