//  OPC-Toolbox
//
//  Copyright (C) 2010 - Allan CORNET  - Maintainer (Scilab 5.x)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// See the file license.txt
//
//=============================================================================
function opc_gui()

  // initialize OPC Client
  opc_server_num = 0;
  opc_server_n = opc_server_browse();
  [namesf, pathlib] = libraryinfo('OPC_clientlib');
  
  TCL_gui_path = pathlib + "../tcl/";

  // initialize TCL GUI
  TCL_SetVar('DIRE', TCL_gui_path);
  TCL_SetVar('DIRE_main', TCL_gui_path);
  
  // start TCL GUI
  TCL_EvalFile(TCL_gui_path + 'OPC.tcl');
endfunction
//=============================================================================
