//  OPC-Toolbox
//
//  Copyright (C) 2010 - Allan CORNET  - Maintainer (Scilab 5.x)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// See the file license.txt
//
// =============================================================================
mode(-1);
lines(0);
// =============================================================================
TOOLBOX_NAME  = "OPC_client";
TOOLBOX_TITLE = "OPC_client";
toolbox_dir   = get_absolute_file_path("builder.sce");
// =============================================================================
// Check Scilab's version
// =============================================================================

try
    v = getversion("scilab");
catch
    error(gettext("Scilab 5.3 or more is required."));
end

if v(2) < 3 then
    // new API in scilab 5.3
    error(gettext('Scilab 5.3 or more is required.'));
end
clear v;

// Check modules_manager module availability
// =============================================================================

if ~isdef('tbx_build_loader') then
  error(msprintf(gettext('%s module not installed."), 'modules_manager'));
end

// Action
// =============================================================================
tbx_builder_gateway(toolbox_dir);
tbx_builder_help(toolbox_dir);
tbx_builder_macros(toolbox_dir);
tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);

// Clean variables
// =============================================================================

clear toolbox_dir TOOLBOX_NAME TOOLBOX_TITLE;

