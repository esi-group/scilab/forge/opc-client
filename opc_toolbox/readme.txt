OPC Client: OLE for Process Control Toolbox for Scilab

Authors:
  Zhe Peng, pengzhe1113@gmail.com - Zhejiang Univ. China
  Longhua Ma, ma.longhua@gmail.com - Zhejiang Univ. China

Maintener:
  Allan CORNET - Scilab - DIGITEO

Thanks to :
  Eilert Mentzoni,
  Erik Hagberg,
  Morten Indst�y,
  Tor Erik Andersen, students in Engineering Cybernetics from Oslo University College,
  Norway for bugs fix and tests.

Allan CORNET - 2010
