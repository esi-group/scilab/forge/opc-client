// ====================================================================
// Copyright DIGITEO 2010
// Allan CORNET
// ====================================================================
demopath = get_absolute_file_path("OPC_client.dem.gateway.sce");

subdemolist = ["demo OPC Client"             ,"OPC_client.dem.sce";];

subdemolist(:,2) = demopath + subdemolist(:,2);
// ====================================================================
