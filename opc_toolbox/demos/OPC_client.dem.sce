opc_server_name = 'Matrikon.OPC.Simulation.1'
opc_group_name = 'group1'
item(1) = 'ABB.tag123'
item(2) = 'ABB.tag234'
item_num=2
flag='f'
item_index=0
item_value=2.4


found_server = opc_server_browse();
if size(found_server,'*') <> 0 then
  opc_connect(found_server(1))
  opc_item_browse()
  opc_add_group(opc_group_name)
  opc_add_item(item,item_num)
  item_read_value=opc_item_read(item_num,flag)
  opc_item_write(item_index,item_value,flag)
  opc_disconnect()
end